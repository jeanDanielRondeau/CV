const express = require('express');
const app = require('./src/controllers');
const localConfig = require('./src/config/localConfig.js');

app.listen(localConfig.port,function(){
    console.log('Server running on port '+localConfig.port);
});
