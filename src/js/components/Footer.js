/*
 * File to render a test page
 *
 */ 
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Navbar from "./Navbar.js";
import Header from "./Header.js";

@connect((store) => {
	if(store.general.language=="EN"){	
	  return {
	    language: store.general.language,
	    footer: store.footer.EN
	  };
	}else{
		return {
	    language: store.general.language,
	    footer: store.footer.FR
	  };
	}
})

export default class Footer extends React.Component {
  render() {
    const { footer, language } = this.props;
    return (
    	<footer class="clearfix">
				<hr />
			        <div class="text-center center-block">
			            <p class="txt-railway">&copy; Jean-Daniel Rondeau</p>
			            <p class="txt-railway">{footer.contactMe}</p>
			            <a href="https://www.facebook.com/jeandaniel.rondeau.3"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
				        <a href="https://gitlab.com/jeanDanielRondeau"><i id="social-gt" class="fa fa-gitlab fa-3x social"></i></a>
				        <a href="mailto:jrondeau2828@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
					</div>


			<br />
    	</footer>
	);
  }
}