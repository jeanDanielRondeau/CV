/*
 * File to render the home page
 *
 */ 
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Navbar from "./Navbar.js";
import Header from "./Header.js";
import Footer from "./Footer.js";
//import { fetchHome } from "../actions/homeActions.js";

@connect((store) => {
  if(store.general.language=="EN"){
    return {
      home: store.home.EN,
      language: store.general.language 
    };
  }else{
    return {
      home: store.home.FR,
      language: store.general.language 
    };
  }
})

export default class Home extends React.Component {
  componentWillMount() {
    //this.props.dispatch(fetchHome());
  }

  render() {
    const { home, language } = this.props;
    return ( 
      <div>
        <Navbar />
        <main>
          <Header />
          <h1>{home.title}</h1> 
          <Link to="/test">Click me</Link>
          <Footer />
        </main>
      </div>
    );
  }
}