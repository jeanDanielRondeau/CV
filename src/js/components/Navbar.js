/*
 * File to render the navbar
 *
 */ 
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { changeRoute } from "../actions/routeActions";

@connect((store) => {
	if(store.general.language=="EN"){	
	  return {
	  	general: store.general.EN,
	    language: store.general.language,
	    navbar: store.navbar.EN
	  };
	}else{
	  return {
	  	general: store.general.FR,
	    language: store.general.language,
	    navbar: store.navbar.FR
	  };
	}
})

export default class Navbar extends React.Component {
  toggleCollapse(){
  	if(document.getElementById("menu-content").className != ""){
  		document.getElementById("menu-content").className = "";
  	}else{
  		document.getElementById("menu-content").className = "menu-content collapse out";
  	}
  }
  activeCurrentRoute(){	
  	const { general } = this.props;
  	document.getElementById("homeLink").className = 
  		(general.currentRoute.toUpperCase() == "ACCEUIL" || general.currentRoute.toUpperCase() == "HOME" ? "active" : "");
  	document.getElementById("backendLink").className = 
  		(general.currentRoute.toUpperCase() == "BACK-END" ? "active" : "");
  }
  changeRoute(route){
  	this.props.dispatch(changeRoute(route));
  }
  
  render() {
    var { language, navbar, general } = this.props;
    return ( 
		<div class="nav-side-menu" onLoad={this.activeCurrentRoute.bind(this, general.currentRoute)}>
		    <div class="brand"><img src="../../img/BrandLogo.PNG" class="brandLogo" /></div>
			<i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content" onClick={this.toggleCollapse.bind(this)}></i>
		        
		    <div class="menu-list">
		        <ul id="menu-content" class="menu-content collapse out">
		          	<Link to="/" onClick={this.changeRoute.bind(this, 'home')}>
		               	<li id="homeLink">
		               		<i class="fa fa-home fa-lg"></i> {navbar.homeLink}
		              	</li>
					</Link>
					<Link to="/backend" onClick={this.changeRoute.bind(this, 'Back-end')}>
		               	<li id="backendLink">
		               		<i class="fa fa-cogs fa-lg"></i> {navbar.backendLink}
		              	</li>
					</Link>
		        </ul>
		    </div>

		</div>
	);
  }
}