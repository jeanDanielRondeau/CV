/*
 * File to render the Header
 *
 */ 
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import { changeLang } from "../actions/langActions.js";

@connect((store) => {
	if(store.general.language=="EN"){	
	  return {
	  	general:store.general.EN,
	    language: store.general.language
	  };
	}else{
		return {
	    general:store.general.FR,
	    language: store.general.language
	  };
	}
})

export default class Header extends React.Component {
  changeJumbotronImage(){
  	var images = ["url(../../img/header1.jpg)", "url(../../img/header2.jpg)",
  		"url(../../img/header3.jpeg)", "url(../../img/header4.jpg)"];
  	var random = Math.floor(Math.random() * images.length);
  	document.getElementById("headerJumbotron").style.backgroundImage = images[random];
  }
  switchLang(lang){
  	this.props.dispatch(changeLang(lang));
  }
  componentDidMount() {
  	this.changeJumbotronImage();
  }
  render() {
    const { general, language } = this.props;
    return (
    	<header onLoad={this.changeJumbotronImage.bind(this)}>
    		<div class="clearfix">
    			<div class="jumbotron" id="headerJumbotron"></div>
		        <h1 id="pageName">
		            {general.currentRoute}
		            <span class="pull-right">
		            	<span class="changeLang" onClick={this.switchLang.bind(this,"FR")}>Fr</span>/<span class="changeLang" onClick={this.switchLang.bind(this,"EN")}>En</span>
		            </span>
		        </h1>
		        <hr/>
		    </div>
    	</header>
	);
  }
}
