/*
 * File to render the back-end page
 *
 */ 
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Navbar from "./Navbar.js";
import Header from "./Header.js";
import Footer from "./Footer.js";

@connect((store) => {
	if(store.general.language=="EN"){	
	  return {
	    backend: store.backend.EN,
	    language: store.general.language
	  };
	}else{
		return {
	    backend: store.backend.FR,
	    language: store.general.language
	  };
	}
})

export default class Test extends React.Component {
  render() {
    const { backend, language } = this.props;
    return (
    	<div>
    	  <Navbar />
    	  <main>
    	  	<Header />
	      	<h2>{backend.javaTitle}</h2> 
	      	<h3>{backend.descriptionTitle}</h3>
	      	<p class="text-justify">{backend.javaDescription}</p>
	      	<h3>{backend.experienceTitle}</h3>
	      	<p class="text-justify">{backend.javaExperience}</p>
	     	
	     	<h2>{backend.nodeJsTitle}</h2> 
	      	<h3>{backend.descriptionTitle}</h3>
	      	<p class="text-justify">{backend.nodeJsDescription}</p>
	      	<h3>{backend.experienceTitle}</h3>
	      	<p class="text-justify">{backend.nodeJsExperience}</p>

	      	<h2>{backend.cppTitle}</h2> 
	      	<h3>{backend.descriptionTitle}</h3>
	      	<p class="text-justify">{backend.cppDescription}</p>
	      	<h3>{backend.experienceTitle}</h3>
	      	<p class="text-justify">{backend.cppExperience}</p>
	      	<Footer />
	      </main>
	    </div>
	);
  }
}
