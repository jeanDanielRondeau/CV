/*
 * File to render a test page
 *
 */ 
import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import Navbar from "./Navbar.js";
import Header from "./Header.js";
import Footer from "./Footer.js";

@connect((store) => {
	if(store.general.language=="EN"){	
	  return {
	    test: store.test.EN,
	    language: store.general.language
	  };
	}else{
		return {
	    test: store.test.FR,
	    language: store.general.language
	  };
	}
})

export default class Test extends React.Component {
  render() {
    const { test, language } = this.props;
    return (
    	<div>
    	  <Navbar />
    	  <main>
    	  	<Header />
	      	<h1>{test.title}</h1> 
	      	<Link to="/">Click me</Link>
	      	<Footer />
	      </main>
	    </div>
	);
  }
}
