/*
 * File to change the content of the html page
 *
 */ 
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from "./components/Home.js";
import Test from "./components/Test.js";
import Backend from "./components/Backend.js";

import store from "./stores.js";

const app = document.getElementById('app');

ReactDOM.render(
<Provider store={store}>
	<BrowserRouter>
		<Switch>
			<Route exact path="/" component={Home}></Route>
			<Route path="/test" component={Test}></Route>
			<Route path="/backend" component={Backend}></Route>		
		</Switch>
	</BrowserRouter>
</Provider>
,app);

