/*
 * File to create the store with all reducers
 *
 */ 
import { createStore } from "redux";

import reducer from "./reducers";

export default createStore(reducer);