/*
 * File to change the general state of the store
 *
 */ 
export default function reducer(state={
    language:"FR",
    FR:{
      currentRoute:"Acceuil"
    },
    EN:{
      currentRoute:"Home"
    }
  }, action) {
    switch (action.type) {
      case "SWITCH_FR": {
        return {...state, language:"FR"}
      }
      case "SWITCH_EN": {
        return {...state, language:"EN"}
      }
      case "HOME_ROUTE":{
        return {...state, FR:{currentRoute:"Acceuil"}, EN:{currentRoute:"Home"}}
      }
      case "BACKEND_ROUTE":{
        return {...state, FR:{currentRoute:"Back-end"}, EN:{currentRoute:"Back-end"}}
      }
    }
    return state;
}