/*
 * File to change the state of the back-end page store
 *
 */ 
export default function reducer(state={
    FR:{
    	cppDescription:"C++ est un language compilé et orienté objet vraiment performant. "+
    				   "Puisque le language est une extension du C, les deux languages ont "+
    				   "une syntaxe similaire et C++ peut utiliser des fonctions de C. "+
    				   "Puisque le compilateur g++ ne s'occupe pas des pointeurs, c'est la "+
    				   "responsabilité des programmeurs de bien les utiliser et de les supprimer "+
    				   "correctement. Cela fait que le code prend plus de temps à être écrit, "+
    				   "mais permet de créer des applications beaucoup plus performantes.",
      	cppExperience:"J'ai appris le C++ durant mes études et je l'ai pratiqué durant trois "+
      				  "projets. Le premier projet était une application qui exécute des "+
      				  "commandes Linux pour créer des répertoires de fichier. Dans le deuxième, "+
      				  "j'ai programmé quatre algorithmes qui m'a été donné pour compresser des "+
      				  "images. Dans le dernier, j'ai reçu une interface graphique d'un jeu "+
      				  "d'extraterrestre qui combattaient en jouant à Roche, Papier, Ciseaux. "+
      				  "J'ai programmé trois intelligences artificielles que le professeur avait "+
      				  "décrites et j'ai programmé mon intelligence artificielle pour ma race "+
      				  "d'extraterrestres.",
      	cppTitle:"C++",
    	descriptionTitle:"Description",
      	experienceTitle:"Expérience de travail",
      	javaDescription:"Java est un langage compilé et orienté objet qui fonctionne sur "+
      					"n'importe quel système d'opération. Maintenant utilisé par environ 9 "+
      					"millions de développeurs, Java est utilisé pour la plupart des "+
      					"applications serveurs de nos jours et fonctionne même pour les "+
      					"applications Android. Inspiré du C et C++, Java permet le polymorphisme "+
      					"et la plupart des options des langages orienté objet sans avoir à "+
      					"s'occuper des pointeurs puisque le «Garbage Collector» va s'occuper "+
      					"de tous les problèmes de mémoires.",
      	javaExperience:"Présentement, toute mon expérience en Java a été acquise à l'école. "+
      				   "Par contre, c'est l'un des langages que je maîtrise le plus puisque c'est "+
      				   "le premier langage que j'ai appris et la plupart de mes cours de "+
      				   "programmations se basaient sur Java pour voir des concepts comme les "+
      				   "fonctions et l'orienté objet. De plus, j'ai eu un cours d'Android qui "+
      				   "a complétement été expliqué en Java. Mes travaux en Java sont nombreux "+
      				   "et parmi ceux-ci j'ai fait du Java-Web, des applications REST avec "+
      				   "Spring, du React, de la génération de PDF, de l'accès à une base de "+
      				   "données, de multiple «threads» etc. Mon plus gros projet a été de créer "+
      				   "un site web qui permettait aux usagers connectés d'inscrire des données "+
      				   "qui étaient affichées lors de la génération d'un document S.E.L.",
      	javaTitle:"Java",
      	nodeJsDescription:"Node.js est une plateforme logicielle qui permet d'utiliser le "+
      					  "JavaScript du coté serveur. Puisque le language utilisé est le "+
      					  "JavaScript, il est très facile pour les développeurs de «front-end» "+
      					  "d'apprendre comment utiliser le «back-end» de Node.js en plus de "+
      					  "supporter nativement le json. Avec son fonctionnement asynchrone, "+
      					  "Node.js peut exécuter plusieurs requêtes sur le même processus en "+
      					  "même temps sans avoir de blocage. Cela permet d'optimiser les "+
      					  "applications en enlevant les temps morts des processus et en enlevant "+
      					  "les problèmes de synchronisation. De plus, à l'aide du gestionnaire "+
      					  "de paquets NPM, il est très simple de rajouter des librairies pour "+
      					  "optimiser toutes sortes d'applications.",
      	nodeJsExperience:"J'ai appris le Node.js par moi-même parce que j'avais un projet "+
      					 "d'école à faire et mais coéquipiers ont opté pour le Node.js. Grâce "+
      					 "à leur aide et quelques vidéos de tutoriels, j'ai appris à me "+ 
      					 "débrouiller assez pour aider dans un projet qui a duré 15 semaines. "+
      					 "Le projet c'est dérouler en équipe de quatre et nous devions faire un "+
      					 "site web pour l'école qui permet de relier des étudiants avancés avec "+
      					 "des nouveaux étudiants pour arranger des sessions de tutorats. J'ai "+
      					 "principalement travaillé sur le «back-end» pour retourner les données "+
      					 "de notre base de données MySQL et j'ai implémenté une page de "+
      					 "connexion avec Json Web Token. J'ai aussi travaillé sur un autre "+
      					 "projet qui a duré trois semaines avec un groupe de 11 étudiants qui "+
      					 "ressemblait à un mélange de Reddit et Facebook. J'ai principalement "+
      					 "travaillé sur le «back-end» pour créer une api qui retournait les "+
      					 "données de la base de données Cassandra et j'ai aidé mes compagnons "+
      					 "qui ne connaissaient pas beaucoup le Node.js. De plus, ce site est "+
      					 "complètement construit en Node.js puisque cela m'offre une bonne "+
      					 "performance et une façon très simple de me faire une api R.E.S.T.",
      	nodeJsTitle:"Node.js",
      	title:"Back-end"
    },
    EN:{
    	cppDescription:"C++ is an object oriented compiled language with high performance. "+
    				   "Because the language is a C extension, the two languages have a similar "+
    				   "syntax and C++ can use functions from C. Because the compiler g++ "+
    				   "doesn't take care of pointers, it's the responsibility of the "+
    				   "programmers to make use them well and to delete them. Because of that "+
    				   "it takes longer to write C++ but it allows to create high performance "+
    				   "applications.",
      	cppExperience:"I have learned C++ in class and praticed it through three projects. The "+
      				  "first project was to program an application that would execute linux "+
      				  "commands to create file repositories. In the second one, I had to "+
      				  "program four different algorithms that were given to compress images. "+
      				  "In the last one, I received a graphic interface for a game of moving "+
      				  "aliens that fought with a Rock, Paper, Scissors game. I programmed three "+
      				  "artificial intelligence the way the teacher wanted and programmed one "+
      				  "for my aliens.",
      	cppTitle:"C++",
      	descriptionTitle:"Description",
      	experienceTitle:"Work experience",
      	javaDescription:"Java is a compiled object oriented language that works on any operating "+
      				  	"system. Now used by approximately 9 million developers, Java is used "+
      				  	"by most of server applications and is even used for Android "+
      				  	"applications. Inspired by C and C++, Java allows polymorphism and most "+
      				  	"of the options that regular object oriented allows without having to "+
      				  	"worry about pointers because there is a Garbage Collector that takes "+
      				  	"care of any memory leak.",	
      	javaExperience:"For now, all my experience in Java as been acquired at school. But, it's "+
      				   "still one of the languages that I master the most because it's the first "+
      				   "language I have learned and most of my programming classes were based on "+
      				   "Java to teach concepts like functions and object oriented programming. "+
      				   "Also, I had Android classes that were entirely in Java. My Java projects "+
      				   "are numerous and among, them I have done some Java-Web, REST applications "+
      				   "with Spring, React applications, generated PDFs, accessed databases, "+
      				   "multiple threads, etc. My biggest project was a website that allowed to "+
      				   "connected users to write data that would be showed when they generated "+
      				   "S.E.L. document in a PDF format.",	  	
      	javaTitle:"Java",
      	nodeJsDescription:"Node.js is a run-time environment qui for executing JavaScript code "+
      					  "server-side. Because the code is JavaScript, it is easy for front-end "+
      					  "developers to learn how to use the back-end in Node.js in addition "+
      					  "to natively supporting the json. With its asynchronous event-driven "+
      					  "architecture, Node.js can execute requests on the same process at "+
      					  "the same time without blockage. This allows to optimise applications "+
      					  "by removing time outs of the process and removing synchronisation "+
      					  "problems. Also, with the help of the package manager NPM, it is "+
      					  "really easy to add libraries to optimise all sorts of applications.",
      	nodeJsExperience:"I learned Node.js by myself because I had a project to do at school "+
      					 "and my teammates requested Node.js. With their help and video "+
      					 "tutorials, I learned enough to work on a project that lasted 15 weeks. "+
      					 "The project was in a team of four and we had to do a website for the "+
      					 "school to match junior students with seniors to make tutoring "+
      					 "sessions. I mostly worked on the back-end to make an api to get the "+
      					 "MySQL database data and I implemented the login feature with Jason "+
      					 "Web Token. I also worked on another project that lasted three weeks "+
      					 "with a team of 11 students where we did a website that looked like a "+
      					 "matchup between Reddit and Facebook. I mostly worked on the back-end "+
      					 "to create an api that connected to our Cassandra database and I helped "+
      					 "my teammates that didn't understand the language as much as me. Also, "+
      					 "this whole website is made in Node.js because it offers great "+
      					 "performance and an easy way to create a R.E.S.T. api.",
      	nodeJsTitle:"Node.js",
      	title:"Back-end"
    }
  }, action) {
    return state;
}