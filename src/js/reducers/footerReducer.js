/*
 * File to change the state of the footer store
 *
 */ 
export default function reducer(state={
    FR:{
      contactMe:"N'hésitez pas à me contacter par courriel ou à regarder le code source" 
    },
    EN:{
      contactMe:"Don't hesitate to contact me by email or to take a look at the source code"
    }
  }, action) {
    return state;
}