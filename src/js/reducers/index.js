/*
 * File to combine every reducers
 *
 */ 
import { combineReducers } from "redux";

import backend from "./backendReducer.js";
import footer from "./footerReducer.js";
import general from "./generalReducer.js";
import home from "./homeReducer.js";
import navbar from "./navbarReducer.js";
import test from "./testReducer.js";

export default combineReducers({
  backend,
  footer,
  general,
  home,
  navbar,
  test
});