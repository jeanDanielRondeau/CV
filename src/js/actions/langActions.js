/*
 * Function to change the language of the general store
 *
 */ 
export function changeLang(lang) {
  if(lang == "EN"){
    return {
      type: "SWITCH_EN"
    };
  }else{
    return {
      type: "SWITCH_FR"
    };
  }
}