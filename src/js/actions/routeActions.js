/*
 * Function to change the route of the general store
 *
 */ 
export function changeRoute(route) {
  if(route.toUpperCase() == "BACK-END"){  	
    return {
      type: "BACKEND_ROUTE"
    };
  }else{
    return {
      type: "HOME_ROUTE"
    };
  }
}