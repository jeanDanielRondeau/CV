module.exports = {
	entry: './src/js/index.js',
	output: {
		filename: 'bundle.js',
		path: __dirname+"/src/"
	},
	module: {
    	loaders: [
		    {
			    test: /\.jsx?$/,
			    exclude: /(node_modules|bower_components)/,
			    loader: 'babel-loader',
			    query: {
			        presets: ['react', 'es2015', 'stage-0'],
			        plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy']
			    }
		    }
	    ]
 	},
	devServer:{
		compress:true,
		disableHostCheck:true
	}	
};
